import {Box, Button, Grid, MenuItem, Select, TextField} from "@mui/material";
import {useEffect, useState} from "react";
import {unique} from "./arrays";
import {API_URL} from "./App";

export default function Admin({token}: { token: string }) {
    const [loading, setLoading] = useState(0);
    const [number, setNumber] = useState<number | undefined>(undefined);
    const [numberError, setNumberError] = useState<string | undefined>(undefined);
    const [buttons, setButtons] = useState<number[]>(() => {
        const saved = localStorage.getItem("buttons");
        if (saved === null || saved === undefined) return [10, 20, 30];
        try {
            return JSON.parse(saved) || [];
        } catch (e) {
            return [];
        }
    });
    const [transactions, setTransactions] = useState<{ class: string, amount: number }[]>(() => {
        const saved = localStorage.getItem("transactions");
        if (saved === null || saved === undefined) return [];
        try {
            return JSON.parse(saved) || [];
        } catch (e) {
            return [];
        }
    });
    const [classes, setClasses] = useState<any[]>([]);
    const [selectedClass, setSelectedClass] = useState<string>('');
    const [countries, setCountries] = useState<any[]>([]);
    const [country, setCountry] = useState<string>('');

    const doTransfer = (points: number) => {
        fetch(`${API_URL}/transfers`, {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                token,
                class_id: selectedClass,
                country_id: country,
                amount: points
            })
        }).then(r => console.log(r));
        setTransactions([
            {
                class: `${classes.find((element => element.id === selectedClass)).name}`,
                amount: points
            },
            ...transactions
        ]);
    }

    useEffect(() => {
        setLoading(loading + 1);
        fetch(`${API_URL}/classes`, {
            mode: 'cors',
            credentials: 'include'
        })
            .then((response) => response.json().then(data => setClasses(data)))
            .then(() => setLoading(loading - 1))
            .catch(() => setLoading(loading - 1));
        setLoading(loading + 1);
        fetch(`${API_URL}/countries`, {
            mode: 'cors',
            credentials: 'include'
        })
            .then((response) => response.json().then(data => setCountries(data)))
            .then(() => setLoading(loading - 1))
            .catch(() => setLoading(loading - 1));
    }, []);
    useEffect(() => {
        localStorage.setItem("buttons", JSON.stringify(buttons));
    }, [buttons]);

    useEffect(() => {
        localStorage.setItem("transactions", JSON.stringify(transactions));
    }, [transactions]);

    if (loading > 0) {
        return <p>Loading...</p>;
    }

    return (
        <Grid container spacing={2}>
            <Grid item xs={8}>
                <TextField variant='standard' label='Amount' type='number' fullWidth value={number}
                           onChange={event => setNumber(Number.parseInt(event.target.value))}
                           error={numberError !== undefined}
                           helperText={numberError}
                />
            </Grid>
            <Grid item xs={4} mt={1}>
                <Button variant='contained' size='medium' fullWidth onClick={() => {
                    if (number === undefined) {
                        setNumberError('Required value.');
                        return;
                    }
                    setButtons(unique([
                        ...buttons,
                        number
                    ]));
                    setNumberError(undefined);
                }}>+</Button>
            </Grid>
            <Grid item xs={6}>
                <Select
                    value={selectedClass}
                    onChange={event => setSelectedClass(event.target.value)}
                    fullWidth
                >{classes
                    .map((data, i) => (
                        <MenuItem key={data.id} value={data.id}>{data.name}</MenuItem>
                    ))}
                </Select>
            </Grid>
            <Grid item xs={6}>
                <Select
                    value={country}
                    onChange={event => setCountry(event.target.value)}
                    fullWidth
                >{countries
                    .map((data, i) => (
                        <MenuItem key={data.id} value={data.id}>{data.name}</MenuItem>
                    ))}
                </Select>
            </Grid>
            {buttons
                .sort((a, b) => a - b)
                .map((number, i) => (
                    <Grid item xs={6} md={4} lg={2} xl={1} key={i}>
                        <Button variant='contained' color="success" size='large' fullWidth
                                onClick={() => doTransfer(number)}>
                            <h2>+ {number} b.</h2>
                        </Button>
                    </Grid>
                ))}
            <Grid item xs={12} mt={1}>
                <Button variant='contained' color='error' size='medium' fullWidth
                        onClick={() => setButtons(unique([]))}>Clear buttons</Button>
            </Grid>
            <Grid item xs={12}>
                <h3>History</h3>
                <Box sx={{height: '30vh', maxHeight: '30vh', overflowY: 'scroll'}}>
                    {transactions
                        .map((data, i) => (
                            <pre key={i}>{data.class}: {data.amount > 0 ? '+' : ''}{data.amount} b.</pre>
                        ))}
                </Box>
            </Grid>
        </Grid>
    );
}
