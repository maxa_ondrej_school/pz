import React from 'react';
import {Route, Routes, useSearchParams} from "react-router-dom";
import Leaderboard from "./Leaderboard";
import Admin from "./Admin";
import NoMatch from "./NoMatch";

export const API_URL = 'https://ahojmaturanti.marrrt.in/api';

export default function App() {
    let [searchParams] = useSearchParams();
    const token = searchParams.get('token');
    if (token === null) {
        return <b>Not allowed!</b>;
    }

    return (
        <Routes>
            <Route index element={<Leaderboard />}/>
            <Route path='admin' element={<Admin token={token}/>}/>
            <Route path="*" element={<NoMatch/>}/>
        </Routes>
    );
}
