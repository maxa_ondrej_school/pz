import {useEffect, useState} from "react";
import {API_URL} from "./App";
import {Box, Button, Typography} from "@mui/material";

export default function Leaderboard() {
    const [winners, setWinners] = useState<string[]>([]);

    useEffect(() => {
        fetch(`${API_URL}/classes`, {
            mode: 'cors',
            credentials: 'include'
        })
            .then((response) => response.json().then(classes => {
                fetch(`${API_URL}/transfers`, {
                    mode: 'cors',
                    credentials: 'include'
                })
                    .then((response) => response.json().then(data => {
                        let parsed = new Map<string, number>();
                        data.forEach((datum: any) => {
                            if (parsed.has(datum.class_id)) {
                                parsed.set(datum.class_id, parsed.get(datum.class_id) + datum.amount);
                            } else {
                                parsed.set(datum.class_id, datum.amount);
                            }
                        });
                        setWinners(
                            Array.from(parsed.entries())
                                .sort((a, b) => b[1] - a[1])
                                .map((datum): string => classes.find((element: any) => element.id === datum[0])?.name)
                        );
                        setTimeout(() => window.location.reload(), 10000);
                    }));
            }));
    }, []);
    return (
        <div>
            <Box sx={{height: '40vh'}}>
                <Button variant='contained' color='success' fullWidth><h1>1. místo</h1></Button>
                <Typography variant='h3' align='center' pt={2}>{winners[0]}</Typography>
            </Box>
            <Box sx={{height: '30vh'}}>
                <Button variant='contained' color='secondary' fullWidth><h2>2. místo</h2></Button>
                <Typography variant='h4' align='center' pt={2}>{winners[1]}</Typography>
            </Box>
            <Box sx={{height: '20vh'}}>
                <Button variant='contained' color='warning' fullWidth><h3>3. místo</h3></Button>
                <Typography variant='h5' align='center' pt={2}>{winners[2]}</Typography>
            </Box>
        </div>
    );
}